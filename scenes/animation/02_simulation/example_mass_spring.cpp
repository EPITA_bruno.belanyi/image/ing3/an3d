
#include "example_mass_spring.hpp"


#ifdef SCENE_MASS_SPRING_1D

using namespace vcl;


static void set_gui(timer_basic& timer);


/** Compute spring force applied on particle pi from particle pj */
vec3 spring_force(const vec3& pi, const vec3& pj, float L0, float K)
{
    vec3 const pji = pj - pi;
    float const L = norm(pji);
    return K * (L - L0) * pji / L;
}


void scene_model::setup_data(std::map<std::string,GLuint>& , scene_structure& , gui_structure& )
{
    // Initial position and speed of particles
    // ******************************************* //
    L0 = 0.008f; // Rest length between each element

    for (size_t i = 0; i < 50; ++i) {
        particles.push_back(particle_element{{i * 1.25f * L0, 0, 0}, {0, 0, 0}});
        forces.push_back(vec3{0, 0, 0});
    }


    // Display elements
    // ******************************************* //
    segment_drawer.init();
    segment_drawer.uniform_parameter.color = {0,0,1};

    sphere = mesh_primitive_sphere();
    sphere.uniform.transform.scaling = 0.005f;


    std::vector<vec3> borders_segments = {{-1,-1,-1},{1,-1,-1}, {1,-1,-1},{1,1,-1}, {1,1,-1},{-1,1,-1}, {-1,1,-1},{-1,-1,-1},
                                          {-1,-1,1} ,{1,-1,1},  {1,-1,1}, {1,1,1},  {1,1,1}, {-1,1,1},  {-1,1,1}, {-1,-1,1},
                                          {-1,-1,-1},{-1,-1,1}, {1,-1,-1},{1,-1,1}, {1,1,-1},{1,1,1},   {-1,1,-1},{-1,1,1}};
    borders = borders_segments;
    borders.uniform.color = {0,0,0};

}





void scene_model::frame_draw(std::map<std::string,GLuint>& shaders, scene_structure& scene, gui_structure& )
{
    timer.update();
    set_gui(timer);


    // Simulation time step (dt)
    float dt = timer.scale*0.050f;

    // Simulation parameters
    const float m  = 0.001f;        // particle mass
    const float K  = 10.0f;         // spring stiffness
    const float mu = 0.50f;       // damping coefficient

    const vec3 g   = {0,-9.81f,0}; // gravity

    // Forces
    const vec3 f_weight =  m * g;

    // Numerical Integration (Verlet)
    for (size_t i = 1; i < particles.size(); ++i)
    {
        vec3& p = particles[i].p; // position of particle
        vec3& v = particles[i].v; // speed of particle

        const vec3 f_spring_prev  = spring_force(p, particles[i - 1].p, L0, K);
        const vec3 f_spring_next  = (i + 1 < particles.size()
                ? spring_force(p, particles[i + 1].p, L0, K)
                : vec3{});

        const vec3 f_curve_prev =  (i >= 2
                ? spring_force(p, particles[i - 2].p, 2 * L0, K)
                : vec3{});
        const vec3 f_curve_next =  (i + 2 < particles.size()
                ? spring_force(p, particles[i + 2].p, 2 * L0, K)
                : vec3{});

        const vec3 f_double_curve_prev =  (i >= 3
                ? spring_force(p, particles[i - 3].p, 3 * L0, K)
                : vec3{});
        const vec3 f_double_curve_next = (i + 3 < particles.size()
                ? spring_force(p, particles[i + 3].p, 3 * L0, K)
                : vec3{});

        const vec3 f_damping = -mu * v;

        forces[i] = f_spring_prev+f_spring_next
            + f_curve_prev+f_curve_next
            + f_double_curve_prev+f_double_curve_next
            +f_weight
            +f_damping;
    }

    for (size_t i = 1; i < particles.size(); ++i) {
        vec3& p = particles[i].p; // position of particle
        vec3& v = particles[i].v; // speed of particle
        v += dt * forces[i];
        p = p + dt * v;
    }


    // Display of the result

    // particle origin
    sphere.uniform.transform.translation = particles[0].p;
    sphere.uniform.color = {0,0,0};
    draw(sphere, scene.camera, shaders["mesh"]);
    // particles and links
    for (size_t i = 1; i < particles.size(); ++i) {
        sphere.uniform.transform.translation = particles[i].p;
        sphere.uniform.color = {1,0,0};
        draw(sphere, scene.camera, shaders["mesh"]);

        segment_drawer.uniform_parameter.p1 = particles[i - 1].p;
        segment_drawer.uniform_parameter.p2 = particles[i].p;
        segment_drawer.draw(shaders["segment_im"],scene.camera);
    }

    draw(borders, scene.camera, shaders["curve"]);
}


/** Part specific GUI drawing */
static void set_gui(timer_basic& timer)
{
    // Can set the speed of the animation
    float scale_min = 0.05f;
    float scale_max = 100.0f;
    ImGui::SliderScalar("Time scale", ImGuiDataType_Float, &timer.scale, &scale_min, &scale_max, "%.2f s");

    // Start and stop animation
    if (ImGui::Button("Stop"))
        timer.stop();
    if (ImGui::Button("Start"))
        timer.start();

}



#endif
