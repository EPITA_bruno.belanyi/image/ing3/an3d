
#include "sphere_collision.hpp"

#include <random>

#ifdef SCENE_SPHERE_COLLISION

using namespace vcl;







void scene_model::frame_draw(std::map<std::string,GLuint>& , scene_structure& scene, gui_structure& )
{
    float dt = 0.02f * timer.scale;
    timer.update();

    set_gui();

    create_new_particle();
    compute_time_step(dt);

    display_particles(scene);
    draw(borders, scene.camera);

}

void do_box_collision(particle_structure& p)
{
    // we know the box's borders...
    const auto alpha = 0.9;
    const auto beta = 0.8;
    vec3 v_dims{alpha, alpha, alpha};
    bool bounced = false;
    for (const auto dim : {0, 1, 2}) {
        const float delta = 1 - std::abs(p.p[dim]);
        if (delta > p.r)
            continue;
        const float overlap = p.r - delta;
        // Keep inside box (inaccurate because of incoming direction)
        // Better would be to use incoming direction and come back earlier
        // Even better yet would be to interpolate previous particle position
        p.p[dim] -= overlap * (p.p[dim] < 0 ? -1 : 1);
        // Do bounce
        v_dims[dim] = -beta;
        bounced = true;
    }

    if (bounced)
        p.v *= v_dims;
}

void do_sphere_collision(particle_structure& p1, particle_structure& p2)
{
    const auto delta = p1.p - p2.p;
    if (norm(delta) > (p1.r + p2.r))
        return;

    // Take care of inertia
    const auto alpha = 0.9; // Friction
    const auto beta = 0.8; // Elastic

    auto u = delta / norm(delta);
    auto j = dot((p2.v - p1.v), u); // m1 == m2 == 1

    p1.v = alpha * p1.v + beta * j * u;
    p2.v = alpha * p2.v - beta * j * u;

    // take care of position
    auto overlap = (normalize(delta) * (p1.r + p2.r)) - delta;
    p1.p += overlap / 2;
    p2.p -= overlap / 2;
}

void scene_model::compute_time_step(float dt)
{
    // Set forces
    const size_t N = particles.size();
    for(size_t k=0; k<N; ++k)
        particles[k].f = vec3(0,-9.81f,0);


    // Integrate position and speed of particles through time
    for(size_t k=0; k<N; ++k) {
        particle_structure& particle = particles[k];
        vec3& v = particle.v;
        vec3& p = particle.p;
        vec3 const& f = particle.f;

        v = (1-0.9f*dt) * v + dt * f; // gravity + friction force
        p = p + dt * v;
    }

    // Collisions with cube
    for (size_t k = 0; k < N; ++k)
        do_box_collision(particles[k]);

    // Collisions between spheres
    for (size_t k = 0; k < N; ++k) {
        particle_structure& p1 = particles[k];
        for (size_t i = 0; i < N; ++i) {
            if (i == k)
                continue;
            particle_structure& p2 = particles[i];
            do_sphere_collision(p1, p2);
        }
    }
}


void scene_model::create_new_particle()
{
    // Emission of new particle if needed
    timer.periodic_event_time_step = gui_scene.time_interval_new_sphere;
    const bool is_new_particle = timer.event;
    static const std::vector<vec3> color_lut = {{1,0,0},{0,1,0},{0,0,1},{1,1,0},{1,0,1},{0,1,1}};

    if( is_new_particle && gui_scene.add_sphere)
    {
        particle_structure new_particle;

        new_particle.r = 0.08f;
        new_particle.c = color_lut[int(rand_interval()*color_lut.size())];

        // Initial position
        new_particle.p = vec3(0,0,0);

        // Initial speed
        const float theta = rand_interval(0, 2*3.14f);
        new_particle.v = vec3( 2*std::cos(theta), 5.0f, 2*std::sin(theta));

        particles.push_back(new_particle);

    }
}
void scene_model::display_particles(scene_structure& scene)
{
    const size_t N = particles.size();
    for(size_t k=0; k<N; ++k)
    {
        const particle_structure& part = particles[k];

        sphere.uniform.transform.translation = part.p;
        sphere.uniform.transform.scaling = part.r;
        sphere.uniform.color = part.c;
        draw(sphere, scene.camera);
    }
}




void scene_model::setup_data(std::map<std::string,GLuint>& shaders, scene_structure& , gui_structure& )
{
    sphere = mesh_drawable( mesh_primitive_sphere(1.0f));
    sphere.shader = shaders["mesh"];

    std::vector<vec3> borders_segments = {{-1,-1,-1},{1,-1,-1}, {1,-1,-1},{1,1,-1}, {1,1,-1},{-1,1,-1}, {-1,1,-1},{-1,-1,-1},
                                          {-1,-1,1} ,{1,-1,1},  {1,-1,1}, {1,1,1},  {1,1,1}, {-1,1,1},  {-1,1,1}, {-1,-1,1},
                                          {-1,-1,-1},{-1,-1,1}, {1,-1,-1},{1,-1,1}, {1,1,-1},{1,1,1},   {-1,1,-1},{-1,1,1}};
    borders = segments_gpu(borders_segments);
    borders.uniform.color = {0,0,0};
    borders.shader = shaders["curve"];

}



void scene_model::set_gui()
{
    // Can set the speed of the animation
    ImGui::SliderFloat("Time scale", &timer.scale, 0.05f, 2.0f, "%.2f s");
    ImGui::SliderFloat("Interval create sphere", &gui_scene.time_interval_new_sphere, 0.05f, 2.0f, "%.2f s");
    ImGui::Checkbox("Add sphere", &gui_scene.add_sphere);

    bool stop_anim  = ImGui::Button("Stop"); ImGui::SameLine();
    bool start_anim = ImGui::Button("Start");

    if(stop_anim)  timer.stop();
    if(start_anim) timer.start();
}





#endif
